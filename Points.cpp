//Include our classes
#include "Points.h"

SmallDots::SmallDots(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE/4;
	mpRect.h = TILE_SIZE/4;

	mpAlive = true;
	mpPoints = 10;

	mpColour = ofColor(255, 255, 255);
	mpGraphicImg = new ofImage;
	mpGraphicImg->load("img/smallDots.png");
	mpCurrentFrameTime = 0;
	mpFrame = 0;
}

void SmallDots::init(int x, int y, int w, int h) {
	SmallDots();
	setXY(x, y);
	setW(w);
	setH(h);
}

SmallDots::~SmallDots(){
}


void SmallDots::update(){
	if (!mpAlive) { return; }
	updateGraphic();
	return;
}


void SmallDots::render(){

	if (!mpAlive) { return; }
	ofSetColor(255,255,255);
	mpGraphicImg->drawSubsection(mpRect.x - (TILE_SIZE/4), mpRect.y - (TILE_SIZE / 4), TILE_SIZE, TILE_SIZE, TILE_SIZE*mpFrame, 0);

	return;
}


// Getters and Setters
void SmallDots::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}

void SmallDots::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void SmallDots::setX(int x) {
	mpRect.x = x;
	return;
}

void SmallDots::setY(int y) {
	mpRect.y = y;
	return;
}

void SmallDots::setW(int w) {
	mpRect.w = w;
	return;
}

void SmallDots::setH(int h) {
	mpRect.h = h;
	return;
}

void SmallDots::setColor(ofColor colour) {
	mpColour = colour;
	return;
}

void SmallDots::setAlive(bool alive) {
	mpAlive = alive;
	return;
}
void SmallDots::setPoints(int points) {
	mpPoints = points;
	return;
}

void SmallDots::updateGraphic() {
	mpCurrentFrameTime += global_delta_time;
	if (mpCurrentFrameTime > 100) {
		mpCurrentFrameTime = 0;
		mpFrame++;
		if (mpFrame >= 10) {
			mpFrame = 0;
		}
	}
	return;
}


C_Rectangle SmallDots::getRect() {
	return mpRect;
}

int SmallDots::getX() {
	return mpRect.x;
}

int SmallDots::getY() {
	return mpRect.y;
}

int SmallDots::getW() {
	return mpRect.w;
}

int SmallDots::getH() {
	return mpRect.h;
}

bool SmallDots::getAlive() {
	return mpAlive;
}
int SmallDots::getPoints() {
	return mpPoints;
}