//Include our classes
#include "Player.h"

Player::Player(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;
	mpSpeed = 200;
	mpDirection = NONE;
	mpMoving = false;

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpGraphicImg = new ofImage;
	mpGraphicImg->loadImage("img/pacman.png");
	mpCurrentFrameTime = 0;
	mpFrame = 0;

}

Player::~Player(){
}



//-------------------------------------------------------------------------------
/*
	Setters
*/

void Player::init() {
	// Memeces varias
	return;
}

void Player::setXY(int x, int y) {
	setX(x);
	setY(y);
	
	return;
}

void Player::setX(int x) {
	mpRect.x = x;
	return;
}

void Player::setY(int y) {
	mpRect.y = y;
	return;
}

void Player::setSpeed(int n) {
	mpSpeed = n;

	return;
}

void Player::setRectangle(C_Rectangle rect) {
	mpRect = rect;
	return;
}

void Player::setW(int w) {
	mpRect.w = w;
	mpRect.h = w;
	return;
}

void Player::setCollisionMap(std::vector<std::vector<bool>>*collisionMap) {
	mpCollisionMap = collisionMap;
	return;
}

void Player::restartPosition() {
	setXY(9 * TILE_SIZE, 15 * TILE_SIZE);
	x_to_go = mpRect.x;
	y_to_go = mpRect.y;
	mpDirection = NONE;
	updateGraphic();
	return;
}

void Player::updateGraphic() {
	mpCurrentFrameTime += global_delta_time;
	if (mpCurrentFrameTime > 60) {
		mpCurrentFrameTime = 0;
		mpFrame++;
		if (mpFrame >= 6) {
			mpFrame = 0;
		}
	}

	int row = mpDirection;

	mpGraphicRect.x = mpFrame * mpGraphicRect.w;
	mpGraphicRect.y = row * mpGraphicRect.h;

	return;
}

/*
	Getters
*/

C_Rectangle Player::getRectangle() {
	return mpRect;
}

int Player::getX() {
	return mpRect.x;
}
int Player::getY() {
	return mpRect.y;
}


int Player::getW() {
	return mpRect.w;
}

int Player::getSpeed() {
	return mpSpeed;
}


/*
	Update and Render
*/

void Player::updateControls() {
	if (mpMoving) {
		return;
	}
	else {
		if ( (key_down['w']) || (key_down['W']) ) {
			mpDirection = UP;
		} else if ( (key_down['s']) || (key_down['S'])) {
			mpDirection = DOWN;
		} else if ( (key_down['a']) || (key_down['A'])) {
			mpDirection = LEFT;
		} else if ( (key_down['d']) || (key_down['D'])) {
			mpDirection = RIGHT;
		}
		else {
			mpDirection = NONE;
		}

		x_to_go = mpRect.x;
		y_to_go = mpRect.y;
		switch (mpDirection) {
		case UP:
			x_to_go = mpRect.x;
			y_to_go = mpRect.y - TILE_SIZE;
			mpMoving = true;
			break;

		case DOWN:
			x_to_go = mpRect.x;
			y_to_go = mpRect.y + TILE_SIZE;
			mpMoving = true;
			break;

		case RIGHT:
			x_to_go = mpRect.x + TILE_SIZE;
			y_to_go = mpRect.y;
			mpMoving = true;
			break;

		case LEFT:
			x_to_go = mpRect.x - TILE_SIZE;
			y_to_go = mpRect.y;
			mpMoving = true;
			break;

		default:
			break;
		}
	}

	return;
}

void Player::move() {
	if (mpMoving) {
		int x_aux = mpRect.x;
		int y_aux = mpRect.y;
				
		if (mpRect.x < x_to_go) {
			mpRect.x += mpSpeed * global_delta_time / 1000;
		}
		else if (mpRect.x > x_to_go) {
			mpRect.x -= mpSpeed * global_delta_time / 1000;
		}

		if (mpRect.y < y_to_go) {
			mpRect.y += mpSpeed * global_delta_time / 1000;
		}
		else if (mpRect.y > y_to_go) {
			mpRect.y -= mpSpeed * global_delta_time / 1000;
		}

		if ((x_aux < x_to_go && mpRect.x > x_to_go) || (x_aux > x_to_go && mpRect.x < x_to_go)) {
			mpRect.x = x_to_go;
		}
		if ((y_aux < y_to_go && mpRect.y > y_to_go) || (y_aux > y_to_go && mpRect.y < y_to_go)) {
			mpRect.y = y_to_go;
		}

		if (mpRect.x == x_to_go && mpRect.y == y_to_go) {
			mpMoving = false;
		}
	}

	return;
}

bool Player::checkCollisionWithMap() {
	if (mpDirection == NONE) { return false; }
	int xx = x_to_go / TILE_SIZE;
	int yy = y_to_go / TILE_SIZE;

	if (yy < 0 || yy >= (*mpCollisionMap).size()) {
		return true;
	}

	if (xx < 0) {
		mpRect.x = ((*mpCollisionMap)[0].size() - 1) * TILE_SIZE;
		x_to_go = mpRect.x;
		return false;
	}

	if (xx >= (*mpCollisionMap)[0].size()) {
		mpRect.x = 0;
		x_to_go = mpRect.x;
		return false;
	}

	return ((*mpCollisionMap)[yy][xx]);
}

void Player::update(){
	updateGraphic();
	updateControls();
	if (!checkCollisionWithMap()) {
		move();
	}
	else {
		mpMoving = false;
	}
	return;	
}

void Player::render(){
	ofSetColor(255,255,255);
	
	mpGraphicImg->drawSubsection(mpRect.x, mpRect.y, mpGraphicRect.w, mpGraphicRect.h, mpGraphicRect.x, mpGraphicRect.y);

	return;	
}


bool Player::isOfClass(std::string classType){
	if(classType == "Player"){
		return true;
	}
	return false;
}

