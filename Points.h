#ifndef POINTS_H
#define POINTS_H

#include "includes.h"

class SmallDots
{
	public:
		SmallDots();
		void init(int x, int y, int w, int h);
		~SmallDots();

		void update();
		void render();

		//Setter and Getter
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		void setAlive(bool alive);
		void setPoints(int points);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();
		
		bool getAlive();
		int getPoints();

		void setColor(ofColor colour);

	protected:

		void updateGraphic();

		ofImage* mpGraphicImg;

	private:
		C_Rectangle mpRect; //Pad Rect
		ofColor mpColour;	//Pad Colour

		int mpPoints;
		bool mpAlive;
		int mpFrame;
		int mpCurrentFrameTime;
};

#endif
