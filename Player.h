#ifndef PLAYER_H
#define PLAYER_H

#include "includes.h"

class Player
{
	public:
		Player();
		~Player();

		void init();
		void update();
		void render();

		// Setters and Getters
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setSpeed(int n);
		void setRectangle(C_Rectangle rect);
		void restartPosition();

		int getX();
		int getY();
		int getW();
		int getSpeed();
		C_Rectangle getRectangle();
		void setCollisionMap(std::vector<std::vector<bool>>*collisionMap);

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Template";};

	protected:
		void updateControls();
		bool checkCollisionWithMap();
		void move();

	
	private:
		C_Rectangle mpRect;

		void updateGraphic();

		C_Rectangle mpGraphicRect;
		ofImage* mpGraphicImg;

		int mpFrame;
		int mpCurrentFrameTime;


		int mpSpeed;
		int mpDirection;
		bool mpMoving;
		int x_to_go;
		int y_to_go;

		std::vector<std::vector<bool>>* mpCollisionMap;
	
};

#endif
