//Include our classes
#include "BigPoints.h"

BigDots::BigDots(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE/2;
	mpRect.h = TILE_SIZE/2;

	mpAlive = true;
	mpPoints = 50;

	mpColour = ofColor(255, 255, 255);
	mpGraphicImg = new ofImage;
	mpGraphicImg->load("img/bigDots.png");
	mpCurrentFrameTime = 0;
	mpFrame = 0;
}

void BigDots::init(int x, int y, int w, int h) {
	BigDots();
	setXY(x, y);
	setW(w);
	setH(h);
}

BigDots::~BigDots(){
}


void BigDots::update(){
	if (!mpAlive) { return; }
	updateGraphic();
	return;
}


void BigDots::render(){

	if (!mpAlive) { return; }
	ofSetColor(255,255,255);
	mpGraphicImg->drawSubsection(mpRect.x - (TILE_SIZE / 4), mpRect.y - (TILE_SIZE / 4), TILE_SIZE, TILE_SIZE, TILE_SIZE*mpFrame, 0);

	return;
}

void BigDots::updateGraphic() {
	mpCurrentFrameTime += global_delta_time;
	if (mpCurrentFrameTime > 100) {
		mpCurrentFrameTime = 0;
		mpFrame++;
		if (mpFrame >= 9) {
			mpFrame = 0;
		}
	}
	return;
}

// Getters and Setters
void BigDots::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}

void BigDots::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void BigDots::setX(int x) {
	mpRect.x = x;
	return;
}

void BigDots::setY(int y) {
	mpRect.y = y;
	return;
}

void BigDots::setW(int w) {
	mpRect.w = w;
	return;
}

void BigDots::setH(int h) {
	mpRect.h = h;
	return;
}

void BigDots::setColor(ofColor colour) {
	mpColour = colour;
	return;
}

void BigDots::setAlive(bool alive) {
	mpAlive = alive;
	return;
}

void BigDots::setPoints(int points) {
	mpPoints = points;
	return;
}

C_Rectangle BigDots::getRect() {
	return mpRect;
}

int BigDots::getX() {
	return mpRect.x;
}

int BigDots::getY() {
	return mpRect.y;
}

int BigDots::getW() {
	return mpRect.w;
}

int BigDots::getH() {
	return mpRect.h;
}

bool BigDots::getAlive() {
	return mpAlive;
}

int BigDots::getPoints() {
	return mpPoints;
}