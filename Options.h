#ifndef OPTIONS_H
#define OPTIONS_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"

enum Selection { SONG, VOLUME, QUIT };

class Options
{
	public:
		Options();
		~Options();

		void update();
		void render();

		void setQuit(bool set);
		bool getQuit();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Options";};

	protected:


	private:
		int mpCurrentOption;
		int mpCurrentTime;
		int mpSong;
		int mpVolume;
		bool mpQuit;
		ofImage* mpArrow;
		C_Rectangle* mpRect;
		ofTrueTypeFont* mpFont_title;
		ofTrueTypeFont* mpFont_text;
		
		std::vector <ofSoundPlayer*> mpSongs;


	
};

#endif
