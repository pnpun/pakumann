#ifndef BIGPOINTS_H
#define BIGPOINTS_H

#include "includes.h"

class BigDots
{
	public:
		BigDots();
		void init(int x, int y, int w, int h);
		~BigDots();

		void update();
		void render();

		//Setter and Getter
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		void setAlive(bool alive);
		void setPoints(int points);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();
		
		bool getAlive();
		int getPoints();
		void updateGraphic();

		ofImage* mpGraphicImg;

		void setColor(ofColor colour);

	protected:
	
	private:
		C_Rectangle mpRect; //Pad Rect
		ofColor mpColour;	//Pad Colour

		int mpPoints;
		bool mpAlive;
		int mpFrame;
		int mpCurrentFrameTime;
};

#endif
