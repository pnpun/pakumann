//Include our classes
#include "Scene.h"

Scene::Scene(){
	
}

Scene::~Scene(){
}

void Scene::init() {

	// Player
	mpPlayer = new Player();
	mpPlayer->init();

	// Enemies
	for (int i = 0; i < ORANGE + 1; i++) {
		Enemy* mpEnemy = new Enemy();
		mpEnemy->setType(i);
		mpEnemies.push_back(mpEnemy);
	}

	// Map
	initMap();

	mpPlayer->setCollisionMap(&mpCollisionMap);
	for (int i = 0; i < ORANGE + 1; i++) {
		mpEnemies[i]->setCollisionMap(&mpCollisionMap);
	}

	mpOffsetX = 0;
	mpOffsetY = 0;
	mpScene = MENU;
	mpMenu = 0;
	mpWait = 0;

	// Menu
	mpFont_menu = new ofTrueTypeFont;
	mpFont_menu->loadFont("aa_arcadefont.ttf", 30);

	mpFont_text = new ofTrueTypeFont;
	mpFont_text->loadFont("aa_pacfont.ttf", 72);

	mpMenuCount = 0;
	mpMenuBox = new ofImage;
	mpMenuBox->loadImage("img/menu_box.png");

	mpOptions = new Options;
	mpScore = new Score();
	
	//Sounds
	mpSoundIntro = new ofSoundPlayer;
	mpSoundIntro->load("sound/pacman_intro.wav");
	mpSoundEatDot = new ofSoundPlayer;
	mpSoundEatDot->load("sound/pacman_eat.wav");
	mpSoundEatGhost = new ofSoundPlayer;
	mpSoundEatGhost->load("sound/pacman_eatghost.wav");
	mpSoundDeath = new ofSoundPlayer;
	mpSoundDeath->load("sound/pacman_death.wav");
	mpSoundEatBigDot = new ofSoundPlayer;
	mpSoundEatBigDot->load("sound/pacman_BigDot.wav");

	return;
}

void Scene::initMap() { // Write the 'Map.txt' file
	int x = 0;
	int y = 0;

	mpLevel = 1;
	mpGraphicImg = new ofImage;
	mpGraphicImg->load("img/wall.png");

	//Points
	mpDotsEaten = 0;
	mpDotsToEat = 0;

	char character = ' ';
	std::string line;
	fstream file;
	file.open("map.txt", std::ios::in);

	if (!file.is_open()) {
		std::cout << "ERROR: Map file not found." << std::endl;
		system("pause");
		exit(0);
	}
	else {
		std::getline(file, line);
		x = stoi(line);

		std::getline(file, line);
		y = stoi(line);

		mpCollisionMap.resize(y);
		mpWallMap.resize(y);
		for (int i = 0; i < y; i++) {
			mpCollisionMap[i].resize(x);
			mpWallMap[i].resize(x);
		}


		for (int i = 0; i < y; i++) {
			std::getline(file, line);
			for (int j = 0; j < x; j++) {
				character = line[j];
				switch (character) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '#':
				case '$':
				case '%':
				case '&':
					mpCollisionMap[i][j] = true;
					mpWallMap[i][j] = character;
					break;
				case 'P':
					mpPlayer->setXY(j * TILE_SIZE, i * TILE_SIZE);
					break;
				//Small Dots
				case '.':
				{
					SmallDots* aDot = new SmallDots();
					aDot->init(j * TILE_SIZE + TILE_SIZE / 4, i * TILE_SIZE + TILE_SIZE / 4, TILE_SIZE/4,TILE_SIZE/4);//init

					mpSmallDots.push_back(aDot);
					mpDotsToEat++;
						break;
				}
				//Big Dots
				case 'O':
				{
					BigDots* aBigDot = new BigDots();
					aBigDot->init(j * TILE_SIZE + TILE_SIZE / 4, i * TILE_SIZE + TILE_SIZE / 4, TILE_SIZE / 2, TILE_SIZE / 2);//init

					mpBigDots.push_back(aBigDot);
					mpDotsToEat++;
					break;
				}

				// RED 'R' | BLUE 'B' | PINK 'S' | ORANGE 'T'
				case 'R':
					mpEnemies[RED]->setXY(j * TILE_SIZE, i * TILE_SIZE);
					mpEnemies[RED]->mpGraphicImg->load("img/red_enemy.png");
					break;

				case 'B':
					mpEnemies[BLUE]->setXY(j * TILE_SIZE, i * TILE_SIZE);
					mpEnemies[BLUE]->mpGraphicImg->load("img/blue_enemy.png");
					break;

				case 'S':
					mpEnemies[PINK]->setXY(j * TILE_SIZE, i * TILE_SIZE);
					mpEnemies[PINK]->mpGraphicImg->load("img/pink_enemy.png");
					break;

				case 'T':
					mpEnemies[ORANGE]->setXY(j * TILE_SIZE, i * TILE_SIZE);
					mpEnemies[ORANGE]->mpGraphicImg->load("img/orange_enemy.png");
					break;

				default:
					break;
				}
			}
		}

		file.close();
	}
	

	return;
}

void Scene::restartMap() {
	int size = mpSmallDots.size();
	for (int i = 0; i < size; i++) {
		mpSmallDots[i]->setAlive(true);
	}
	mpDotsEaten = 0;

	mpPlayer->restartPosition();

	size = mpBigDots.size();
	for (int i = 0; i < size; i++) {
		mpBigDots[i]->setAlive(true);
	}
	mpWait = 0;

	return;
}

void Scene::renderMap() {
	ofSetColor(255,255,255);
	int sizeV = mpWallMap.size();
	int sizeH = 0;
	if (sizeV > 0) {
		sizeH = mpWallMap[0].size();
	}
	else {
		return;
	}

	for (int i = 0; i < sizeV; i++) {
		for (int j = 0; j < sizeH; j++) {
			if (mpCollisionMap[i][j]) {
				switch (mpWallMap[i][j]) {
				case '0':
					mpFrame = 0;
					break;
				case '1':
					mpFrame = 1;
					break;
				case '2':
					mpFrame = 2;
					break;
				case '3':
					mpFrame = 3;
					break;
				case '4':
					mpFrame = 4;
					break;
				case '5':
					mpFrame = 5;
					break;
				case '6':
					mpFrame = 6;
					break;
				case '7':
					mpFrame = 7;
					break;
				case '8':
					mpFrame = 8;
					break;
				case '9':
					mpFrame = 9;
					break;
				case '#':
					mpFrame = 10;
					break;
				case '$':
					mpFrame = 11;
					break;
				case '%':
					mpFrame = 12;
					break;
				case '&':
					mpFrame = 13;
					break;
				default:
					break;
				}

				mpGraphicImg->drawSubsection(j*TILE_SIZE, i*TILE_SIZE, TILE_SIZE, TILE_SIZE, mpFrame*TILE_SIZE, 0);
			}
		}
	}

	return;
}

void Scene::update(){
	if (!mpOptions->getQuit()) {
		mpOptions->update();
	}
	else {
		switch (mpScene) {
		case MENU:
			mpMenuCount += global_delta_time;
			if (mpMenuCount > 300) {
				if ((key_down['s'] || key_down['S']) && mpMenu < GAMEOVER) {
					mpMenu += 1;
					mpMenuCount = 0;
				}
				else if ((key_down['w'] || key_down['W']) && mpMenu > 0) {
					mpMenu -= 1;
					mpMenuCount = 0;
				}
				else if ((key_down[' '])) {
					mpMenuCount = 0;
					if (mpMenu == 0) {
						mpScene = GAME;
						mpSoundIntro->play();
					}
					else if (mpMenu == 1) {
						mpOptions->setQuit(false);
					}
					else if (mpMenu == 2) {
						exit(0);
					}
				}
			}
			break;

		case GAME:
		{
			if (mpWait > 3000 && !mpSoundIntro->isPlaying()) {
				/*
					INGAME
				*/
				mpPlayer->update();
				mpScore->update();

				C_Rectangle playerRect = mpPlayer->getRectangle();

				int size = 0;
				int bigSize = 0;

				int scoreGot = 0;//Score got

				size = mpSmallDots.size();
				for (int i = 0; i < size;i++) {
					SmallDots* smDot = mpSmallDots[i];
					smDot->update();
					if (smDot->getAlive()) {
						if (C_RectangleCollision(playerRect, smDot->getRect())) {
							smDot->setAlive(false);
							mpDotsEaten++;
							scoreGot += smDot->getPoints();
							if (!mpSoundEatDot->isPlaying()) {
								mpSoundEatDot->play();
							}
						}
					}
				}

				bigSize = mpBigDots.size();
				for (int i = 0; i < bigSize;i++) {

					BigDots* smBigDot = mpBigDots[i];
					smBigDot->update();
					if (smBigDot->getAlive()) {
						if (C_RectangleCollision(playerRect, smBigDot->getRect())) {
							smBigDot->setAlive(false);
							mpDotsEaten++;
							scoreGot += smBigDot->getPoints();

							//Do enemies weakness
							for (int i = 0; i < ORANGE + 1; i++) {
								mpEnemies[i]->setMode(false);
							}
							mpSoundEatBigDot->play();
						}
					}
				}


				for (int i = 0; i < ORANGE + 1; i++) {
					mpEnemies[i]->setPlayerPos(mpPlayer->getX(), mpPlayer->getY());
					mpEnemies[i]->update();

					if (C_RectangleCollision(playerRect, mpEnemies[i]->getRectangle())) {
						if (mpEnemies[i]->getMode()) {
							mpPlayer->restartPosition();

							for (int j = 0; j < ORANGE + 1; j++) {
								mpEnemies[j]->restartPosition();
							}
							mpSoundDeath->play();
							mpScore->livesLose();
							mpWait = 0;
						}
						else {
							mpEnemies[i]->restartPosition();
							scoreGot += mpEnemies[i]->getPoints();
							mpSoundEatGhost->play();
						}
					}
				}

				// Lose condition
				if (mpScore->getLives() <= 0) {
					mpScore->writeFile();
					mpScene = GAMEOVER;
				}

				// Win condition
				if (mpDotsToEat == mpDotsEaten) {
					mpLevel++;
					restartMap();
					for (int i = 0; i < ORANGE + 1; i++) {
						mpEnemies[i]->restartPosition();
						mpEnemies[i]->increaseSpeed(50);
					}
				}

				//For test
				mpScore->addScore(scoreGot);
			}
			else {
				mpWait += global_delta_time;
			}

			break;
		}
		case GAMEOVER:
			if (key_down['r'] || key_down['R']) {
				mpScore->setLives(3);
				mpScene = GAME;
				for (int i = 0; i < ORANGE + 1; i++) {
					mpEnemies[i]->setSpeed(100);
				}
				mpScore->setScore(0);
				for (int i = 0; i < mpSmallDots.size(); i++) {
					mpSmallDots[i]->setAlive(true);
				}
				for (int i = 0; i < mpBigDots.size(); i++) {
					mpBigDots[i]->setAlive(true);
				}
				mpDotsEaten = 0;
				mpLevel = 1;
				mpSoundIntro->play();
			}
			break;

		default:
			break;
		}

	}

	return;
}
void Scene::render(){
	if (!mpOptions->getQuit()) {
		mpOptions->render();
	}
	else {
		switch (mpScene) {
		case MENU: {
			ofSetColor(255, 255, 0);
			mpFont_text->drawString("paku - mann", SCREEN_WIDTH / 16, SCREEN_HEIGHT / 4);
			ofSetColor(255, 255, 255);
			switch (mpMenu)
			{
			case 0:

				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2, 128, 64, 128, 0);
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 128, 128, 64, 0, 0);
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 256, 128, 64, 0, 0);
				mpFont_menu->drawString("PLAY", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 42);
				mpFont_menu->drawString("OPTIONS", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 170);
				mpFont_menu->drawString("QUIT", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 298);
				break;

			case 1:
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2, 128, 64, 0, 0);
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 128, 128, 64, 128, 0);
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 256, 128, 64, 0, 0);
				mpFont_menu->drawString("PLAY", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 42);
				mpFont_menu->drawString("OPTIONS", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 170);
				mpFont_menu->drawString("QUIT", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 298);
				break;

			case 2:
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2, 128, 64, 0, 0);
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 128, 128, 64, 0, 0);
				mpMenuBox->drawSubsection(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 256, 128, 64, 128, 0);
				mpFont_menu->drawString("PLAY", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 42);
				mpFont_menu->drawString("OPTIONS", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 170);
				mpFont_menu->drawString("QUIT", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 298);
				break;

			default:
				break;
			}

			break;
		}
		case GAME: {
			renderMap();

			int size = mpSmallDots.size();
			for (int i = 0; i < size; i++) {
				mpSmallDots[i]->render();
			}

			int bigSize = mpBigDots.size();
			for (int i = 0; i < bigSize; i++) {
				mpBigDots[i]->render();
			}

			for (int i = 0; i < ORANGE + 1; i++) {
				mpEnemies[i]->render();
			}

			mpPlayer->render();

			mpScore->render();

			mpFont_menu->drawString("LEVEL " + itos(mpLevel, 1), 620, 130);
			break;
		}
		case GAMEOVER:
			ofSetColor(255, 255, 0);
			mpFont_text->drawString("GAME OVER", SCREEN_WIDTH / 16, SCREEN_HEIGHT / 4);
			ofSetColor(255, 255, 255);

			mpFont_menu->drawString("Press     R     to     restart", SCREEN_HEIGHT / 2, SCREEN_HEIGHT / 2);

			break;

		default:
			break;
		}
	}
	return;
}

bool Scene::isOfClass(std::string classType){
	if(classType == "Scene"){
		return true;
	}
	return false;
}

