#ifndef ENEMY_H
#define ENEMY_H

#include "includes.h"

class Enemy
{
	public:
		Enemy();
		~Enemy();

		void init();
		void update();
		void render();

		// Setters and Getters
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setSpeed(int n);
		void setRectangle(C_Rectangle rect);
		void setPlayerPos(int x, int y);
		void setType(int type);
		void setCollisionMap(std::vector<std::vector<bool>>*collisionMap);
		void restartPosition();
		void setMode(bool mode);
		void increaseSpeed(int speed);

		int getX();
		int getY();
		int getW();
		int getSpeed();
		C_Rectangle getRectangle();
		int getType();
		int getPoints();
		bool getMode();

		ofImage* mpGraphicImg;
		ofImage* mpGraphicToniImg;

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Template";};

	protected:
		void updateControls();
		bool checkCollisionWithMap();
		void move();
		void orangeAI(int posX, int posY);
		void redAI(int posX, int posY);
		void blueAI(int posX, int posY);
		void pinkAI(int posX, int posY);

		void updateGraphic();

		C_Rectangle mpGraphicRect;

		int mpFrame;
		int mpCurrentFrameTime;

	
	private:
		C_Rectangle mpRect;
		int mpSpeed;
		int mpDirection;
		bool mpMoving;
		int x_to_go;
		int y_to_go;
		int mpType;
		int mpPlayerPosX;
		int mpPlayerPosY;
		int mpPoints;
		bool mpToniMode; // You better run

		int mpCurrTime;

		std::vector<std::vector<bool>>* mpCollisionMap;
	
};

#endif
