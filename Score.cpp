//Include our classes
#include "Score.h"

Score::Score(){
	//Scores
	mpFont_score = new ofTrueTypeFont();
	mpFont_score->loadFont("aa_arcadefont.ttf", 30);
	mpScore = 0;
	//Highscores
	mpHighScore = 0;
	mpRecord = false;
	readHighScore();
	
	//Lifes
	mpLives = 3;
	mpAlive = true;
	mpLifeImage = new ofImage;
	mpLifeImage->load("img/life.png");

	mpSoundHighScore = new ofSoundPlayer;
	mpSoundHighScore->load("sound/pacman_extrapac.wav");
	mpNewHighScore = false; // If the player makes a new highscore, play a sound
}

Score::~Score(){
}

void Score::update(){

	compareScores();

	if (mpLives == 0) {
		mpAlive = false;
	}

}

void Score::render(){
	std::string score;
	score = itos(mpScore, 1);
	mpFont_score->drawString("SCORE " + score, 620, 50);

	std::string highScore;
	highScore = itos(mpHighScore, 1);
	mpFont_score->drawString("HIGH SCORE " + highScore, 620, 90);

	for (int i = 0; i < mpLives; i++) {
		mpLifeImage->draw(620 + 44*i, 150);
	}

	return;
}

void setColor(ofColor colour) {

}

void Score::setScore(int score) {
	mpScore = score;
	return;
}

int Score::getScore() {
	return mpScore;
}


bool Score::isOfClass(std::string classType){
	if(classType == "Score"){
		return true;
	}
	return false;
}

void Score::addScore(int points) {
	mpScore += points;
	return;
}

/*
	HighScore
*/

void Score::compareScores() {
	if (mpHighScore == 0) {
		mpNewHighScore = true;
	}
	if (mpScore > mpHighScore) {
		if (!mpNewHighScore) {
			mpNewHighScore = true;
			mpSoundHighScore->play();
		}
		mpHighScore = mpScore;
	}
	return;
}

// readFile
void Score::readHighScore() {
	std::fstream file;
	file.open("highScore.txt", std::ios::in | std::ios::binary);
	if (!file.is_open()) {
		return;
	}
	else {
		file.read((char*)&mpHighScore, sizeof(int));
		file.close();
	}
}

// writeFile
void Score::writeFile() {
	std::fstream file;
	file.open("highScore.txt", std::ios::out | std::ios::binary | std::ios::trunc);
	file.write((char*)&mpHighScore, sizeof(int));
	file.close();
	return;
}
/*
	Life
*/
void Score::livesLose() {
	mpLives--;
	return;
}

void Score::setAlive(bool alive) {
	mpAlive = alive;
	return;
}
bool Score::getAlive() {
	return mpAlive;
}

void Score::setLives(int lives) {
	mpLives = lives;
	return;
}
int Score::getLives() {
	return mpLives;
}
