//Include our classes
#include "Enemy.h"

Enemy::Enemy(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;
	mpSpeed = 100;
	mpDirection = DOWN;
	mpMoving = false;
	mpToniMode = true;
	mpPoints = 500;
	mpCurrTime = 0;

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpGraphicImg = new ofImage;
	mpCurrentFrameTime = 0;
	mpFrame = 0;

	mpGraphicToniImg = new ofImage;
	mpGraphicToniImg->load("img/rosa_enemy.png");

}

Enemy::~Enemy(){
}



//-------------------------------------------------------------------------------
/*
	Setters
*/

void Enemy::init() {
	return;
}

void Enemy::setXY(int x, int y) {
	setX(x);
	setY(y);
	
	return;
}

void Enemy::setX(int x) {
	mpRect.x = x;
	return;
}

void Enemy::setY(int y) {
	mpRect.y = y;
	return;
}

void Enemy::setSpeed(int n) {
	mpSpeed = n;

	return;
}

void Enemy::setRectangle(C_Rectangle rect) {
	mpRect = rect;
	return;
}

void Enemy::setW(int w) {
	mpRect.w = w;
	mpRect.h = w;
	return;
}

void Enemy::setCollisionMap(std::vector<std::vector<bool>>*collisionMap) {
	mpCollisionMap = collisionMap;
	return;
}

void Enemy::setPlayerPos(int x, int y) {
	mpPlayerPosX = x;
	mpPlayerPosY = y;
	return;
}

void Enemy::setType(int type) {
	mpType = type;
	return;
}

void Enemy::restartPosition() {
	mpRect.x = 9 * TILE_SIZE;
	mpRect.y = 9 * TILE_SIZE;
	x_to_go = mpRect.x;
	y_to_go = mpRect.y;
	setMode(true);
	return;
}

void Enemy::setMode(bool mode) {
	mpCurrTime = 0;
	mpToniMode = mode;
	return;
}

void Enemy::increaseSpeed(int speed) {
	mpSpeed += speed;
	return;
}

void Enemy::updateGraphic() {

	mpCurrentFrameTime += global_delta_time;
	if (mpCurrentFrameTime > 100) {
		mpCurrentFrameTime = 0;
		mpFrame++;
		if (mpFrame >= 6) {
			mpFrame = 0;
		}
	}

	mpGraphicRect.x = mpFrame * mpGraphicRect.w;

	return;
}

/*
	Getters
*/

C_Rectangle Enemy::getRectangle() {
	return mpRect;
}

int Enemy::getX() {
	return mpRect.x;
}
int Enemy::getY() {
	return mpRect.y;
}


int Enemy::getW() {
	return mpRect.w;
}

int Enemy::getSpeed() {
	return mpSpeed;
}

int Enemy::getType() {
	return mpType;
}

int Enemy::getPoints() {
	return mpPoints;
}

bool Enemy::getMode() {
	return mpToniMode;
}


/*
	Update and Render
*/

void Enemy::updateControls() {
	if (mpMoving) {
		return;
	}
	else {
		int posX = mpRect.x / TILE_SIZE;
		int posY = mpRect.y / TILE_SIZE;
		switch (mpType) {
		case RED:
			redAI(posX, posY);
			break;
		case BLUE:
			blueAI(posX, posY);
			break;

		case PINK:
			pinkAI(posX, posY);
			break;

		case ORANGE:
			orangeAI(posX, posY);
			break;
		default:
			break;
		}

		x_to_go = mpRect.x;
		y_to_go = mpRect.y;
		switch (mpDirection) {
		case UP:
			x_to_go = mpRect.x;
			y_to_go = mpRect.y - TILE_SIZE;
			mpMoving = true;
			break;

		case DOWN:
			x_to_go = mpRect.x;
			y_to_go = mpRect.y + TILE_SIZE;
			mpMoving = true;
			break;

		case RIGHT:
			x_to_go = mpRect.x + TILE_SIZE;
			y_to_go = mpRect.y;
			mpMoving = true;
			break;

		case LEFT:
			x_to_go = mpRect.x - TILE_SIZE;
			y_to_go = mpRect.y;
			mpMoving = true;
			break;

		default:
			break;
		}
	}

	return;
}

void Enemy::move() {
	if (mpMoving) {
		int x_aux = mpRect.x;
		int y_aux = mpRect.y;
				
		if (mpRect.x < x_to_go) {
			mpRect.x += mpSpeed * global_delta_time / 1000;
		}
		else if (mpRect.x > x_to_go) {
			mpRect.x -= mpSpeed * global_delta_time / 1000;
		}

		if (mpRect.y < y_to_go) {
			mpRect.y += mpSpeed * global_delta_time / 1000;
		}
		else if (mpRect.y > y_to_go) {
			mpRect.y -= mpSpeed * global_delta_time / 1000;
		}

		if ((x_aux < x_to_go && mpRect.x > x_to_go) || (x_aux > x_to_go && mpRect.x < x_to_go)) {
			mpRect.x = x_to_go;
		}
		if ((y_aux < y_to_go && mpRect.y > y_to_go) || (y_aux > y_to_go && mpRect.y < y_to_go)) {
			mpRect.y = y_to_go;
		}

		if (mpRect.x == x_to_go && mpRect.y == y_to_go) {
			mpMoving = false;
		}
	}

	return;
}

bool Enemy::checkCollisionWithMap() {
	int xx = x_to_go / TILE_SIZE;
	int yy = y_to_go / TILE_SIZE;

	if (yy < 0 || yy >= (*mpCollisionMap).size()) {
		return true;
	}

	if (xx < 0) {
		mpRect.x = ((*mpCollisionMap)[0].size() - 1) * TILE_SIZE;
		x_to_go = mpRect.x;
		return false;
	}

	if (xx >= (*mpCollisionMap)[0].size()) {
		mpRect.x = 0;
		x_to_go = mpRect.x;
		return false;
	}

	return ((*mpCollisionMap)[yy][xx]);
}

/*
	Inteligences
*/

// Random
void Enemy::orangeAI(int posX, int posY) {
	int randValue = 0;
	if (mpDirection == UP || mpDirection == DOWN) {
		if (!(*mpCollisionMap)[posY][posX + 1] && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = LEFT;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && (*mpCollisionMap)[posY][posX - 1] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY][posX + 1] == true && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = LEFT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else {
			if (mpDirection == UP && (*mpCollisionMap)[posY - 1][posX] == true) {
				mpDirection = DOWN;
			}
			else if (mpDirection == DOWN && (*mpCollisionMap)[posY + 1][posX] == true) {
				mpDirection = UP;
			}
		}

	}
	else if (mpDirection == RIGHT || mpDirection == LEFT) {
		if (!(*mpCollisionMap)[posY + 1][posX] && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = DOWN;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && (*mpCollisionMap)[posY - 1][posX] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = DOWN;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY + 1][posX] == true && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (posX < (*mpCollisionMap)[posY].size() - 2 && posX > 1) {
			if (mpDirection == RIGHT && (*mpCollisionMap)[posY][posX + 1] == true) {
				mpDirection = LEFT;
			}
			else if (mpDirection == LEFT && (*mpCollisionMap)[posY][posX - 1] == true) {
				mpDirection = RIGHT;
			}
		}
	}
}

// Follows player
void Enemy::redAI(int posX, int posY) {
	int randValue = 0;
	if (mpDirection == UP || mpDirection == DOWN) {
		if (!(*mpCollisionMap)[posY][posX + 1] && !(*mpCollisionMap)[posY][posX - 1]) {	
			if (mpRect.x < mpPlayerPosX && !(*mpCollisionMap)[posY][posX + 1] && mpRect.y == mpPlayerPosY) {
				randValue = 0;
			} else if (mpRect.x > mpPlayerPosX && !(*mpCollisionMap)[posY][posX - 1] && mpRect.y == mpPlayerPosY) {
				randValue = 1;
			}
			else {
				randValue = rand() % 3;
			}

			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = LEFT;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && (*mpCollisionMap)[posY][posX - 1] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY][posX + 1] == true && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = LEFT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else {
			if (mpDirection == DOWN && (*mpCollisionMap)[posY + 1][posX] == true) {
				mpDirection = UP;
			}
			else if (mpDirection == UP && (*mpCollisionMap)[posY - 1][posX] == true) {
				mpDirection = DOWN;
			}
		}

	}
	else if (mpDirection == RIGHT || mpDirection == LEFT) {
		if (!(*mpCollisionMap)[posY + 1][posX] && !(*mpCollisionMap)[posY - 1][posX]) {
			if (mpRect.y > mpPlayerPosY && !(*mpCollisionMap)[posY - 1][posX] && mpRect.x == mpPlayerPosX) {
				randValue = 0;
			}
			else if (mpRect.y < mpPlayerPosY && !(*mpCollisionMap)[posY + 1][posX] && mpRect.x == mpPlayerPosX) {
				randValue = 1;
			}
			else {
				randValue = rand() % 3;
			}

			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = DOWN;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && (*mpCollisionMap)[posY - 1][posX] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = DOWN;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY + 1][posX] == true && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (posX < (*mpCollisionMap)[posY].size() - 2 && posX > 1) {
			if (mpDirection == RIGHT && (*mpCollisionMap)[posY][posX + 1] == true) {
				mpDirection = LEFT;
			}
			else if (mpDirection == LEFT && (*mpCollisionMap)[posY][posX - 1] == true) {
				mpDirection = RIGHT;
			}
		}
	}
}

// Follows player -> right
void Enemy::blueAI(int posX, int posY) {
	int randValue = 0;
	// Direction
	switch (mpDirection) {
	case UP:
		if (!(*mpCollisionMap)[posY][posX + 1] && mpRect.y == mpPlayerPosY && mpRect.x < mpPlayerPosX) { //Can turn right
				mpDirection = RIGHT;
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = LEFT;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && (*mpCollisionMap)[posY][posX - 1] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY][posX + 1] == true && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = LEFT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else {
			if (mpDirection == UP && (*mpCollisionMap)[posY - 1][posX] == true) {
				mpDirection = DOWN;
			}
			else if (mpDirection == DOWN && (*mpCollisionMap)[posY + 1][posX] == true) {
				mpDirection = UP;
			}
		}
		break;

	case DOWN:
		if (!(*mpCollisionMap)[posY][posX - 1] && mpRect.y == mpPlayerPosY && mpRect.x > mpPlayerPosX) { //Can turn left
				mpDirection = LEFT;
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = LEFT;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && (*mpCollisionMap)[posY][posX - 1] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY][posX + 1] == true && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = LEFT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else {
			if (mpDirection == UP && (*mpCollisionMap)[posY - 1][posX] == true) {
				mpDirection = DOWN;
			}
			else if (mpDirection == DOWN && (*mpCollisionMap)[posY + 1][posX] == true) {
				mpDirection = UP;
			}
		}
		break;

	case RIGHT:
		if (!(*mpCollisionMap)[posY + 1][posX] && mpRect.x == mpPlayerPosX && mpRect.y < mpPlayerPosY) { //Can turn down
				mpDirection = DOWN;
		} else if (!(*mpCollisionMap)[posY + 1][posX] && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = DOWN;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && (*mpCollisionMap)[posY - 1][posX] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = DOWN;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY + 1][posX] == true && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (posX < (*mpCollisionMap)[posY].size() - 2 && posX > 1) {
			if (mpDirection == RIGHT && (*mpCollisionMap)[posY][posX + 1] == true) {
				mpDirection = LEFT;
			}
			else if (mpDirection == LEFT && (*mpCollisionMap)[posY][posX - 1] == true) {
				mpDirection = RIGHT;
			}
		}
		break;

	case LEFT:
		if (!(*mpCollisionMap)[posY - 1][posX] && mpRect.x == mpPlayerPosX && mpRect.y > mpPlayerPosY) { //Can turn down
				mpDirection = UP;
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = DOWN;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && (*mpCollisionMap)[posY - 1][posX] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = DOWN;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY + 1][posX] == true && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (posX < (*mpCollisionMap)[posY].size() - 2 && posX > 1) {
			if (mpDirection == RIGHT && (*mpCollisionMap)[posY][posX + 1] == true) {
				mpDirection = LEFT;
			}
			else if (mpDirection == LEFT && (*mpCollisionMap)[posY][posX - 1] == true) {
				mpDirection = RIGHT;
			}
		}
		break;

	default:
		break;
	}
}

// Follows player -> left
void Enemy::pinkAI(int posX, int posY) {
	int randValue = 0;
	// Direction
	switch (mpDirection) {
	case UP:
		if (!(*mpCollisionMap)[posY][posX - 1] && mpRect.y == mpPlayerPosY && mpRect.x > mpPlayerPosX) { //Can turn left
			mpDirection = LEFT;
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = LEFT;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && (*mpCollisionMap)[posY][posX - 1] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY][posX + 1] == true && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = LEFT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else {
			if (mpDirection == UP && (*mpCollisionMap)[posY - 1][posX] == true) {
				mpDirection = DOWN;
			}
			else if (mpDirection == DOWN && (*mpCollisionMap)[posY + 1][posX] == true) {
				mpDirection = UP;
			}
		}
		break;

	case DOWN:
		if (!(*mpCollisionMap)[posY][posX + 1] && mpRect.y == mpPlayerPosY && mpRect.x < mpPlayerPosX) { //Can turn right
			mpDirection = RIGHT;
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = LEFT;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY][posX + 1] && (*mpCollisionMap)[posY][posX - 1] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = RIGHT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY][posX + 1] == true && !(*mpCollisionMap)[posY][posX - 1]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = LEFT;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else {
			if (mpDirection == UP && (*mpCollisionMap)[posY - 1][posX] == true) {
				mpDirection = DOWN;
			}
			else if (mpDirection == DOWN && (*mpCollisionMap)[posY + 1][posX] == true) {
				mpDirection = UP;
			}
		}
		break;

	case RIGHT:
		if (!(*mpCollisionMap)[posY - 1][posX] && mpRect.x == mpPlayerPosX && mpRect.y > mpPlayerPosY) { //Can turn up
			mpDirection = UP;
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = DOWN;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && (*mpCollisionMap)[posY - 1][posX] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = DOWN;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY + 1][posX] == true && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (posX < (*mpCollisionMap)[posY].size() - 2 && posX > 1) {
			if (mpDirection == RIGHT && (*mpCollisionMap)[posY][posX + 1] == true) {
				mpDirection = LEFT;
			}
			else if (mpDirection == LEFT && (*mpCollisionMap)[posY][posX - 1] == true) {
				mpDirection = RIGHT;
			}
		}
		break;

	case LEFT:
		if (!(*mpCollisionMap)[posY + 1][posX] && mpRect.x == mpPlayerPosX && mpRect.y < mpPlayerPosY) { //Can turn down
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 3;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = DOWN;
				break;
			case 2:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (!(*mpCollisionMap)[posY + 1][posX] && (*mpCollisionMap)[posY - 1][posX] == true) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = DOWN;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if ((*mpCollisionMap)[posY + 1][posX] == true && !(*mpCollisionMap)[posY - 1][posX]) {
			randValue = rand() % 2;
			switch (randValue)
			{
			case 0:
				mpDirection = UP;
				break;
			case 1:
				mpDirection = mpDirection;
				break;
			default:
				break;
			}
		}
		else if (posX < (*mpCollisionMap)[posY].size() - 2 && posX > 1) {
			if (mpDirection == RIGHT && (*mpCollisionMap)[posY][posX + 1] == true) {
				mpDirection = LEFT;
			}
			else if (mpDirection == LEFT && (*mpCollisionMap)[posY][posX - 1] == true) {
				mpDirection = RIGHT;
			}
		}
		break;

	default:
		break;
	}
}



void Enemy::update(){
	updateControls();
	if (!checkCollisionWithMap()) {
		move();
	}
	else {
		mpMoving = false;
	}

	if (!mpToniMode) {
		mpCurrTime += global_delta_time;
		if (mpCurrTime > 10000) {
			setMode(true);
			mpCurrTime = 0;
		}
	}
	return;	
}

void Enemy::render() {
	ofSetColor(255, 255, 255);
	updateGraphic();
	if (mpToniMode) {
		mpGraphicImg->drawSubsection(mpRect.x, mpRect.y, mpGraphicRect.w, mpGraphicRect.h, mpGraphicRect.x, mpGraphicRect.y);
	}
	else {
		mpGraphicToniImg->drawSubsection(mpRect.x, mpRect.y, mpGraphicRect.w, mpGraphicRect.h, mpGraphicRect.x, mpGraphicRect.y);
	}

	
	return;	
}


bool Enemy::isOfClass(std::string classType){
	if(classType == "Player"){
		return true;
	}
	return false;
}

