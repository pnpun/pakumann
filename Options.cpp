//Include our classes
#include "Options.h"

Options::Options(){
	mpCurrentOption = 0;
	mpCurrentTime = 0;
	mpArrow = new ofImage;
	mpArrow->load("img/select_icon.png");
	mpRect = new C_Rectangle;
	mpSong = 0;
	mpVolume = 5;

	// Songs
	mpSongs.resize(4);

	for (int i = 0; i < 4; i++) {
		mpSongs[i] = new ofSoundPlayer;
	}

	mpSongs[0]->load("sound/00.mp3");
	mpSongs[1]->load("sound/01.mp3");
	mpSongs[2]->load("sound/02.mp3");
	mpSongs[3]->load("sound/03.mp3");
	
	for (int i = 0; i < 4; i++) {
		mpSongs[i]->setLoop(true);
	}

	mpFont_title = new ofTrueTypeFont;
	mpFont_title->loadFont("aa_pacfont.ttf", 72);
	mpFont_text = new ofTrueTypeFont;
	mpFont_text->loadFont("aa_arcadefont.ttf", 30);

	mpRect->w = 32;
	mpRect->h = 62;
	mpQuit = true;
}

Options::~Options(){
}

void Options::setQuit(bool set) {
	mpQuit = set;
	return;
}

bool Options::getQuit() {
	return mpQuit;
}

void Options::update(){
	// Get input
	mpCurrentTime += global_delta_time;
	if (mpCurrentTime > 300) {

		if (key_down['s'] || key_down['S'] || key_down['a'] || key_down['A'] || key_down['w'] || key_down['W'] || key_down['d'] || key_down['D'] || key_down[' ']) {
			mpCurrentTime = 0;
		}

		if ((key_down['s'] || key_down['S']) && mpCurrentOption < 2) {
			mpCurrentOption++;
		} else if ((key_down['w'] || key_down['W']) && mpCurrentOption > 0) {
			mpCurrentOption--;
		}
		else if (mpCurrentOption == SONG) {
			if ((key_down['a'] || key_down['A'])) {
				mpSong--;
				if (mpSong < 0) {
					mpSong = 3;
				}
			}
			else if ((key_down['d'] || key_down['D'])) {
				mpSong++;
				if (mpSong > 3) {
					mpSong = 0;
				}
			}
			else if (key_down[' ']) {
				for (int i = 0; i < 4; i++) {
					mpSongs[i]->stop();
				}
				mpSongs[mpSong]->play();
			}
		}
		else if (mpCurrentOption == VOLUME) {
			if ((key_down['a'] || key_down['A']) && mpVolume > 0) {
				mpVolume--;
			}
			else if ((key_down['d'] || key_down['D']) && mpVolume < 5) {
				mpVolume++;
			}
			for (int i = 0; i < 4; i++) {
				mpSongs[i]->setVolume(mpVolume*0.2);
			}
		}
		else if (mpCurrentOption == QUIT) {
			if (key_down[' ']) {
				mpQuit = true;
			}
		}
		

	}






	return;
}
void Options::render(){
	ofSetColor(255, 255, 0);
	mpFont_title->drawString("options", SCREEN_WIDTH / 5, SCREEN_HEIGHT / 4);
	
	
	switch (mpCurrentOption) {

	case SONG:
		ofSetColor(150, 150, 150);
		mpArrow->draw(SCREEN_WIDTH / 2 + 160, SCREEN_HEIGHT / 2);
		mpArrow->mirror(false, true);
		mpArrow->draw(SCREEN_WIDTH / 2 - 216, SCREEN_HEIGHT / 2);
		mpArrow->mirror(false, true);


		ofSetColor(255, 255, 255);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2, 128, 64);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 128, 128, 64);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 256, 128, 64);

		ofSetColor(0, 0, 0);
		mpFont_text->drawString(itos(mpSong + 1, 1), SCREEN_WIDTH / 2 - 74, SCREEN_HEIGHT / 2 + 42);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 124, SCREEN_HEIGHT / 2 + 132, 120, 56);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 124, SCREEN_HEIGHT / 2 + 260, 120, 56);


		break;

	case VOLUME:
		ofSetColor(255, 255, 255);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2, 128, 64);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 128, 128, 64);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 256, 128, 64);

		ofSetColor(0, 0, 0);
		for (int i = 0; i < mpVolume; i++) {
			ofDrawRectangle(SCREEN_WIDTH / 2 - 128 + (4 + (4+20.8)*i), SCREEN_HEIGHT / 2 + 132, 20.8, 56);
		}
		ofDrawRectangle(SCREEN_WIDTH / 2 - 124, SCREEN_HEIGHT / 2 + 4, 120, 56);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 124, SCREEN_HEIGHT / 2 + 260, 120, 56);

		break;

	case QUIT:
		ofSetColor(255, 255, 255);	
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2, 128, 64);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 128, 128, 64);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 128, SCREEN_HEIGHT / 2 + 256, 128, 64);

		ofSetColor(0, 0, 0);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 124, SCREEN_HEIGHT / 2 + 4, 120, 56);
		ofDrawRectangle(SCREEN_WIDTH / 2 - 124, SCREEN_HEIGHT / 2 + 132, 120, 56);

		break;

	default:
		break;
	}

	ofSetColor(255, 255, 255);
	mpFont_text->drawString("SONG", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 42);
	mpFont_text->drawString("VOLUME", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 170);
	mpFont_text->drawString("MENU", SCREEN_WIDTH / 2 + 32, SCREEN_HEIGHT / 2 + 298);


	return;
}



bool Options::isOfClass(std::string classType){
	if(classType == "Template"){
		return true;
	}
	return false;
}

