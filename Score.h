#ifndef SCORE_H
#define SCORE_H

#include "includes.h"
#include "Utils.h"

class Score
{
	public:
		Score();
		~Score();

		void update();
		void render();

		//Setters 'n' getters

		void setScore(int score);
		int getScore();

		void addScore(int points);

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Score";};

		void setColor(ofColor colour);
		
		//Lifes		
		void livesLose();
		
		void setAlive(bool alive);
		bool getAlive();

		void setLives(int lives);
		int getLives();


		bool mpAlive;
		// File
		void readHighScore();
		void writeFile();

	protected:
		void compareScores();

	private:
		int mpLives;
		int mpBigDot;
		int mpSmallDot;

		int mpScore;
		int mpHighScore;

		bool mpRecord;

		ofTrueTypeFont* mpFont_score;

		ofColor mpColour;

		ofImage* mpLifeImage;

		ofSoundPlayer* mpSoundHighScore;
		bool mpNewHighScore;
};

#endif
