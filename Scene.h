#ifndef SCENE_H
#define SCENE_H

#include "includes.h"
#include "Utils.h"
#include "Player.h"
#include "Points.h"
#include "BigPoints.h"
#include "Enemy.h"
#include "Score.h"
#include "Options.h"

class Scene
{
	public:
		Scene();
		~Scene();

		void init();
		void update();
		void render();

		bool isOfClass(std::string classType);			// <-- No hace falta
		std::string getClassName(){return "Scene";};	// <-- No hace falta

	protected:
		// Private functions
		void initMap();
		void renderMap();
		void restartMap();

		// Objects
		Player* mpPlayer;
		Score* mpScore;
		Options* mpOptions;

		
	
	private:
		// Wait
		int mpWait;
		
		// Enemies vector
		std::vector <Enemy*> mpEnemies;

		// Mover el mapa al pintarlo
		int mpOffsetX;
		int mpOffsetY;

		int mpDotsToEat;
		int mpDotsEaten;
		
		std::vector <std::vector<bool>> mpCollisionMap;
		std::vector <std::vector<char>> mpWallMap;

		//Dots Score
		std::vector<SmallDots*> mpSmallDots;
		std::vector<BigDots*> mpBigDots;

		ofTrueTypeFont* mpFont_menu;
		ofTrueTypeFont* mpFont_text;
		ofImage* mpGraphicImg;
		int mpFrame;
		
		//ofTrueTypeFont* mpFont_score;

		int mpLevel;
		int mpScene;
		bool mpNewHighScore;

		int mpMenuCount;
		int mpMenu;
		ofImage* mpMenuBox;

		//Sounds
		ofSoundPlayer* mpSoundIntro;
		ofSoundPlayer* mpSoundEatDot;
		ofSoundPlayer* mpSoundEatGhost;
		ofSoundPlayer* mpSoundDeath;
		ofSoundPlayer* mpSoundEatBigDot;
		ofSoundPlayer* mpSoundHighScore;

};

#endif
